/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 21:34:17 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/21 19:55:35 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*res;
	int		size;
	int		i;

	i = 0;
	size = 0;
	if (s && f)
	{
		while (s[size] != '\0')
			size++;
		res = (char*)malloc(sizeof(*res) * (size) + 1);
		if (res == NULL)
			return (NULL);
		while (i < size)
		{
			res[i] = f(s[i]);
			i++;
		}
		res[size] = '\0';
		return (res);
	}
	return (0);
}
