/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 21:04:10 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 22:21:08 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

void	ft_print_one(char *s1, char *s2)
{
	int		i;

	i = 0;
	if (ft_strcmp(s2, ".") == 0)
		write(1, ".    ", 5);
	else if (ft_strcmp(s2, "..") == 0)
		write(1, "..    ", 6);
	else
	{
		i = ft_strlen(s1) + 1;
		while (i < (int)ft_strlen(s2))
		{
			ft_putchar(s2[i]);
			i++;
		}
		write(1, "    ", 4);
	}
}

void	ft_printf_2d_arr(char **mass, char *name_dir,
								t_options *option, int l)
{
	int		i;

	i = 0;
	if (option->t == true && option->r == true)
		ft_mergesort((void **)mass, l, ft_for_r_sort);
	else if (option->t == true)
		ft_mergesort((void **)mass, l, ft_for_t_sort);
	else if (option->r == true)
		ft_mergesort((void **)mass, l, ft_for_r_sort);
	else if (option->f == true)
		;
	else
		ft_mergesort((void **)mass, l, ft_for_st_sort);
	if (option->l == true)
		ft_ls_l_total(mass);
	while (i < l)
	{
		if (option->l == true)
			ft_final_param(mass[i], name_dir);
		else if (option->one == true)
			ft_str_re_cut(name_dir, mass[i]);
		else
			ft_print_one(name_dir, mass[i]);
		i++;
	}
}

void	ft_str_re_cut(char *s1, char *s2)
{
	int		i;

	if (ft_strcmp(s2, ".") == 0)
		write(1, ".\n", 2);
	else if (ft_strcmp(s2, "..") == 0)
		write(1, "..\n", 3);
	else
	{
		i = ft_strlen(s1) + 1;
		while (i < (int)ft_strlen(s2))
		{
			ft_putchar(s2[i]);
			i++;
		}
		write(1, "\n", 1);
	}
}
