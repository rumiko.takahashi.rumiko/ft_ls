/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tec.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/11 15:58:15 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/11 20:21:50 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

int		ft_for_r_sort(const char *a, const char *b)
{
	return (ft_strcmp(a, b) < 0);
}

int		ft_for_t_sort(const char *a, const char *b)
{
	lstat(a, &sb);
	lstat(b, &sb2);
	return (sb.st_mtime < sb2.st_mtime);
}

int		ft_for_st_sort(const char *a, const char *b)
{
	return (ft_strcmp(a, b) > 0);
}

void	ft_only_first_path(char *name_dir, int i)
{
	if (i == 0)
		;
	else
	{
		write(1, "\n", 1);
		write(1, "\n", 1);
		ft_putstr(name_dir);
		write(1, ":", 1);
		write(1, "\n", 1);
	}
}

int		ft_sort_for_input(const char *a, const char *b)
{
	lstat(a, &sb);
	lstat(b, &sb2);
	if (S_ISREG(sb.st_mode) < S_ISREG(sb2.st_mode))
		return (1);
	else
		return (0);
}
