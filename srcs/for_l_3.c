/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   for_l_3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 20:38:50 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/10 13:20:52 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

void	ft_ls_l_total(char **path)
{
	int		total;

	total = 0;
	while (*path)
	{
		stat(*path, &sb);
		total += sb.st_blocks;
		path++;
	}
	ft_putstr("total ");
	ft_putnbr(total);
	ft_putchar('\n');
}
