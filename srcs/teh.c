/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   teh.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/23 14:23:00 by vbudnik           #+#    #+#             */
/*   Updated: 2018/03/28 22:06:13 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"
#include "../libft/libft.h"

static int		ft_count_all(char *name_dir, t_options *opt)
{
	DIR		*dir;
	int		i;

	i = 0;
	if (!(dir = opendir(name_dir)))
		return (0);
	while ((dp = readdir(dir)) != NULL)
	{
		if (ft_dot(dp->d_name) == 1 && opt->a == false)
			continue ;
		i++;
	}
	closedir(dir);
	return (i);
}

int				ft_free_mass(int l, char **mass, char *name_dir, t_options *opt)
{
	int		i;
	int		k;

	k = ft_count_all(name_dir, opt);
	i = 0;
	l = l + 0;
	while (i < k)
	{
		ft_strdel(&mass[i]);
		i++;
	}
	free(mass);
	return (0);
}

void			ft_print_link(char *s1, char *s2)
{
	int		i;

	i = 0;
	if (ft_strcmp(s2, ".") == 0)
		write(1, ".    ", 5);
	else if (ft_strcmp(s2, "..") == 0)
		write(1, "..    ", 6);
	else
	{
		i = ft_strlen(s1) + 1;
		while (i < (int)ft_strlen(s2))
		{
			ft_putchar(s2[i]);
			i++;
		}
	}
}

void			ft_tech_for_sokrat(char *res)
{
	write(1, res, ft_strlen(res));
	write(1, "\n", 1);
	ft_strdel(&res);
}

void			ft_free_not(char **del, char ***mass)
{
	free(del);
	free(mass);
}
